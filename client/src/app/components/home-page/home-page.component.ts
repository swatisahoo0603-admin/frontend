import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authenication/authenication.service';
import { UserService } from 'src/app/services/user/user.service';
import { UserType } from 'src/app/shared/enums/UserType-enum.model';
import { User } from 'src/app/shared/models/User.model';
import { DismissiveAlertComponent } from '../utils/dismissive-alert/dismissive-alert.component';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css'],
})
export class HomePageComponent implements OnInit {
  user!: User;
  userTypeString!: string;

  @ViewChild(DismissiveAlertComponent)
  alert: DismissiveAlertComponent;

  constructor(
    private userService: UserService,
    private authService: AuthenticationService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.user = this.userService.loggedInUser;
    if (this.user) {
      if (this.user.userType) {
        this.userTypeString = this.user.userType?.toLocaleString();
      }
    }
    if (sessionStorage.getItem('basicauth') === null) {
      this.authService.logOut();
    }
  }

  ngAfterViewInit(): void {
    if (!this.user) {
      this.alert.add(
        'danger',
        'You are not logged in, Please login first',
        5000
      );
      //router navigate to register page here
      this.router.navigate(['login']);
    }
  }

  onLogout(): void {
    this.authService.logOut();
  }
}
