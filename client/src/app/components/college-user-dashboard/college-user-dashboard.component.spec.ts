import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CollegeUserDashboardComponent } from './college-user-dashboard.component';

describe('CollegeUserDashboardComponent', () => {
  let component: CollegeUserDashboardComponent;
  let fixture: ComponentFixture<CollegeUserDashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CollegeUserDashboardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CollegeUserDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
