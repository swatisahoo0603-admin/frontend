import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { JobService } from 'src/app/services/job/job.service';
import { UserService } from 'src/app/services/user/user.service';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { User } from 'src/app/shared/models/User.model';
import { Job } from 'src/app/shared/models/Job.model';
import { DismissiveAlertComponent } from '../../utils/dismissive-alert/dismissive-alert.component';

@Component({
  selector: 'app-college-jobs-detail',
  templateUrl: './college-jobs-detail.component.html',
  styleUrls: ['./college-jobs-detail.component.css'],
})
export class CollegeJobsDetailComponent implements OnInit {
  jobs: Job[] = [];
  applied: any = [];
  nojobs: any;
  posted: any = [];
  deleted: any;
  errormsg: any;
  successmsg: boolean;
  jobinfo: any;
  jobdata: any = {};
  ProfileForm: FormGroup;
  updated: any;
  allJobsOfCollege: Job[] = [];
  allAppliedJobsByLoggedInUser: Job[] = [];
  userCollegeId: any;
  //jobs:Job[];
  //user: User;
  user: User = this.userService.loggedInUser;

  @ViewChild(DismissiveAlertComponent)
  alert!: DismissiveAlertComponent;

  constructor(
    private router: Router,
    private activeroute: ActivatedRoute,
    private jobService: JobService,
    private userService: UserService
  ) { }

  ngOnInit(): void {
    this.userCollegeId = this.userService.loggedInUser.userCollege?.collegeId;
    this.getjobs();
    this.appliedjobs();
    this.getAllAppliedJobsByLoggedInUser();
  }

  getjobs() {
    this.jobService.getjobList().subscribe((data) => {
      this.jobs = data;
    });
  }

  onApply(job: any) {
    console.log(job);
    //this.jobService.applyjob(job).subscribe((response: any) => {
    this.alert.add('info', 'Job applied successfully', 2000);
    this.allJobsOfCollege.push(job);
    this.allAppliedJobsByLoggedInUser.push(job);
    this.jobService.applyjob(job).subscribe({
      next: (result) => {
        console.log(job);
        //this.allJobsOfCollege.push(result);
        //this.allAppliedJobsByLoggedInUser.push(result);

      },
      error: (err) => {
        console.log(err);
        console.log(job.value);
        // this.alert.add('danger', 'Could not apply to job', 8000);
      },
      complete: () => console.info('complete'),
    });
  }

  getAllAppliedJobsByLoggedInUser() {
    this.jobService.getAppliedJobsByLoggedInUser().subscribe({
      next: (result) => {
        console.log('LoggedinUser jobs', this.allAppliedJobsByLoggedInUser);
        this.allAppliedJobsByLoggedInUser = result;
        this.alert.add('info', 'My Applied Jobs fetched  successfully', 2000);
      },
      error: (err) => {
        console.log('LoggedinUser jobs', err);
        this.alert.add('danger', 'Could not apply to job', 8000);
      },
      complete: () => console.info('complete'),
    });
  }

  appliedjobs() {
    this.jobService.getappliedjobsbyusers(this.user.userCollege!).subscribe({
      next: (result: any) => {
        this.allJobsOfCollege = result;
        this.alert.add('info', 'Applied jobs by college fetched', 2000);
      },
      error: (err) => {
        console.log(err);
        this.alert.add(
          'danger',
          'Posted Jobs by company  could not be fetched',
          8000
        );
      },
      complete: () => console.info('complete'),
    });
  }
}
