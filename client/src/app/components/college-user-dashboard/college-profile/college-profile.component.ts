import { Component, Input, OnInit, Output, ViewChild } from '@angular/core';
import { CollegeService } from 'src/app/services/college/college.service';
import { UserService } from 'src/app/services/user/user.service';
import { College } from 'src/app/shared/models/College.model';
import { User } from 'src/app/shared/models/User.model';
import { UserType } from 'src/app/shared/enums/UserType-enum.model';
import { BehaviorSubject, Subject } from 'rxjs';
import { DismissiveAlertComponent } from '../../utils/dismissive-alert/dismissive-alert.component';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-college-profile',
  templateUrl: './college-profile.component.html',
  styleUrls: ['./college-profile.component.css'],
})
export class CollegeProfileComponent implements OnInit {
  // Store college details of user | Dummy object for testing
  college: College;

  // Json Stringify version of college object for passing in form| Dummy object for testing
  formCollege: College;

  user: User = this.userService.loggedInUser;

  // Get from parent controllers
  collegeIdFromUser: number;

  // Check for UserType
  disableSwitching: boolean;

  // List of SPoC users | Dummy object for testing
  allUsers: User[];
  buttonNameUpdate: string = 'Update';

  constructor(
    private collegeService: CollegeService,
    private userService: UserService
  ) {}

  @ViewChild(DismissiveAlertComponent)
  alert!: DismissiveAlertComponent;

  ngOnInit(): void {
    // Will get collegeId from user
    if (
      this.userService.loggedInUser &&
      this.userService.loggedInUser.userCollege &&
      this.userService.loggedInUser.userCollege.collegeId
    ) {
      this.collegeIdFromUser =
        this.userService.loggedInUser.userCollege.collegeId;
    }

    this.collegeService.getCollegeById(this.collegeIdFromUser).subscribe({
      next: (result) => {
        // console.log(result);
        this.college = result;
        //send a copy to user-detail-form
        this.formCollege = JSON.parse(JSON.stringify(this.college));
        this.alert.add('info', 'College details fetched', 2000);
      },
      error: (err) => {
        this.alert.add('danger', 'College details could not be fetched', 8000);
      },
      complete: () => console.info('complete'),
    });

    // Get all college users
    this.collegeService.getAllCollegeUsers(this.collegeIdFromUser).subscribe({
      next: (result) => {
        this.allUsers = result;
        this.alert.add('info', 'SPoC details fetched', 2000);
      },
      error: (err) => {
        console.log(err);
        //this.alert.add('danger', 'SPoC details could not be fetched', 8000);
      },
      complete: () => console.info('complete'),
    });

    // Check if admin or not (By Default - false)
    if (this.userService.loggedInUser.userType?.toLocaleString() == 'COLLEGE_ADMIN') {
      this.disableSwitching = true;
    }
  }

  onUpdate(college: College) {
    // alert('Works!');
    this.collegeService.updateCollege(this.formCollege).subscribe({
      next: (result) => {
        this.college = result;
        this.alert.add('success', 'College details updated sucessfully', 2000);
        if (this.userService.loggedInUser.userType?.toLocaleString() == 'COLLEGE_ADMIN') {
          this.disableSwitching = true;
        }
      },
      error: (err) => {
        this.alert.add('danger', 'College details could not be updated', 3000);
      },
      complete: () => console.info('complete'),
    });
  }
  approveStatus(spocUser: User) {
    if (spocUser.userAccountId) {
      this.collegeService
        .approveSpocApproval(spocUser.userAccountId)
        .subscribe({
          next: (result) => {
            //this.college = result;
            this.alert.add('success', 'Account Approved', 2000);
            spocUser.adminApprovalStatus = true;
            if (this.userService.loggedInUser.userType?.toLocaleString() == 'COLLEGE_ADMIN') {
              this.disableSwitching = true;
            }
          },
          error: (err) => {
            console.log(err);
            this.alert.add('danger', 'Error occurred while approving', 3000);
          },
          complete: () => console.info('complete'),
        });
    }
  }
}
