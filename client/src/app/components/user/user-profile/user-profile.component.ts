import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authenication/authenication.service';
import { UserService } from 'src/app/services/user/user.service';
import { User } from 'src/app/shared/models/User.model';
import { DismissiveAlertComponent } from '../../utils/dismissive-alert/dismissive-alert.component';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css'],
})
export class UserProfileComponent implements OnInit {
  user!: User;
  formUser!: User;
  constructor(private userService: UserService,private router:Router,private authService:AuthenticationService) {}

  //Alert Component whose add method is gonna be accesed here
  @ViewChild(DismissiveAlertComponent)
  alert!: DismissiveAlertComponent;

  ngOnInit(): void {
    this.user = this.userService.loggedInUser;
    this.formUser = JSON.parse(JSON.stringify(this.user));
  }

  onUpdate(user: any) {
    this.userService.updateUser(this.formUser).subscribe({
      next: (res) => {
        this.user = res;
        this.alert.add('success', 'User details updated sucessfully', 2000);
      },
      error: (err) => {
        this.alert.add('danger', 'User details could not be updated', 3000);
      },
      complete: () => console.info('complete'),
    });
  }

  onLogout() {
    // this.userService.logoutUser().subscribe({
    //   next: (res) => {
    //     //redirection
    //     console.log('Loggedout');
    //   },
    //   error: (err) => {
    //     this.alert.add('danger', 'A problem occured while logging out', 3000);
    //   },
    //   complete: () => console.info('complete'),
    // });
    this.authService.logOut();
    // this.router.navigate(['login']);
  }
}

//user-detail-view--->emit(changedData)--->user-profile---backend--->chnaged user in user-profile---->child
//shalow   collegeUser     [...]   jsonParse(jsStrinfigfy) object.assign
//cdep

//endpoints
