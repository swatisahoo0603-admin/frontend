import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Job } from 'src/app/shared/models/Job.model';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { JobService } from 'src/app/services/job/job.service';
import { Router } from '@angular/router';
import { Location } from 'src/app/shared/models/Location.model';
import { LocationService } from 'src/app/services/location/location.service';
import { Qualification } from 'src/app/shared/models/Qualification.model';
import { QualificationService } from 'src/app/services/qualification/qualification.service';
import { SkillSet } from 'src/app/shared/models/SkillSet.model';
import { SkillsetService } from 'src/app/services/skillset/skillset.service';
import { JobDomain } from 'src/app/shared/enums/JobDomain-enum.model';
import { JobType } from 'src/app/shared/enums/JobType-enum.model';
import { jobOpenFor } from 'src/app/shared/enums/JobOpenFor-enum.model';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { NgSelectModule } from '@ng-select/ng-select';

@Component({
  selector: 'app-job-detail-form',
  templateUrl: './job-detail-form.component.html',
  styleUrls: ['./job-detail-form.component.css'],
})
export class JobDetailFormComponent implements OnInit {
  @Input()
  job!: Job;

  formjob: Job;
  postjobForm: FormGroup;
  postedMsg: any;
  alreadyposted: any;
  jobinfo: any;
  jobdata: any = {};
  ProfileForm: FormGroup;
  updated: any;
  routeState: any;
  locations: Location[] = [];
  qualifications:Qualification[]=[];
  skills:SkillSet[]=[];

  @Output()
  formSubmit: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private jobService: JobService,
    private locationService: LocationService,
    private qualificationService:QualificationService,
    private skillsetService:SkillsetService
  ) {}


  updateForm: FormGroup;

  cat=[{ locationId:1, locationCity:''}];
  categories=[{}];
  selected: any;
  multiple: boolean;
  getSelectedValue() {
    console.log(this.selected);
  }
  
  cat_skills=[{ skillSetId:1, skillSetName:''}];
  categories_skills = [{}];
  selected_skills: any;
  multiple_skills: boolean;
  getSelectedSkills() {
    console.log(this.selected_skills);
  }

  cat_qualification=[{ qualificationId:1, qualificationName:''}]
  categories_qualifications = [{}];
  selected_qualifications: any;
  multiple_qualifications: boolean;
  getSelectedQualifications() {
    console.log(this.selected_qualifications);
  }


  ngOnInit() {
    //this.jobs=history.state;
    this.locationService.getAllLocations().subscribe((res) => {
      console.log("fetchedLocations",res);
      this.locations = res;
      //let map=new Map<number,string>();
      for(let i=0;i<this.locations.length;i++){
        this.locations[i].locationCity=this.locations[i].locationCity+','+this.locations[i].locationState+','+this.locations[i].locationCountry;
        this.cat.push({'locationId':this.locations[i].locationId!,'locationCity':this.locations[i].locationCity!});
        this.categories=this.cat;
      }
      });

       this.qualificationService.getAllQualifications().subscribe((res) => {
         console.log("fetchedQualifications",res);
         this.qualifications = res;
         for(let i=0;i<this.qualifications.length;i++){
          console.log(this.qualifications[i].qualificationSpecialization);
          this.qualifications[i].qualificationName=this.qualifications[i].qualificationName+','+this.qualifications[i].qualificationSpecialization!;
           this.cat_qualification.push({'qualificationId':this.qualifications[i].qualificationId!,'qualificationName':this.qualifications[i].qualificationName!});
           this.categories_qualifications=this.cat_qualification;
         }
         });

        this.skillsetService.getAllSkillSet().subscribe((res) => {
         console.log("fetchedSkills",res);
         this.skills = res;
         //let map=new Map<number,string>();
         for(let i=0;i<this.skills.length;i++){
           this.cat_skills.push({'skillSetId':this.skills[i].skillSetId!,'skillSetName':this.skills[i].skillSetName!});
           this.categories_skills=this.cat_skills;
         }
        });

    this.postjobForm = this.formBuilder.group({
      jobRole: [''],
      jobDescription: [''],
      jobDomain: [''],
      jobType: [''],
      jobOpenFor: [''],
      jobLocation: [''],
      qualification: [''],
      skillSet: [''],
    });

    if (this.job) {
      this.postjobForm.patchValue(this.job);
    }
  }
  onFormSubmit(): void {
    this.formSubmit.emit(this.postjobForm);
  }

  // compareQualification(o1: any, o2: any) {
  //   if(o1.qualificationId == o2.qualificationId )
  //   return true;
  //   else return false
  // }

  compareQualification(item: any, selected: any) {
    return item.qualificationId === selected.qualificationId;
  }

  compareSkill(item: any, selected: any) {
    return item.skillSetId === selected.skillSetId;
  }

  compareLocation(item: any, selected: any) {
    return item.locationId === selected.locationId;
  }
}
