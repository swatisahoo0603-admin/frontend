import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { Company } from 'src/app/shared/models/Company.model';

@Component({
  selector: 'app-company-detail-form',
  templateUrl: './company-detail-form.component.html',
  styleUrls: ['./company-detail-form.component.css'],
})
export class CompanyDetailFormComponent implements OnInit {
  @Input()
  company!: Company;

  @Input()
  buttonName!: string;

  @Output()
  formSubmit: EventEmitter<any> = new EventEmitter<any>();

  constructor() {}

  ngOnInit(): void {}

  onFormSubmit(): void {
    this.formSubmit.emit(this.company);
  }

  // onRegister(): void {
  //   this.companyService.registerCompany(this.updateForm.value).subscribe({
  //     next: (result) => {
  //       this.company = result;
  //       this.alert.add('success', 'Company details registered sucessfully', 2000);
  //     },
  //     error: (err) => {
  //       this.alert.add('danger', 'Error', 3000);
  //     }
  //   })
  // }
}
