import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DismissiveAlertComponent } from './dismissive-alert.component';

describe('DismissiveAlertComponent', () => {
  let component: DismissiveAlertComponent;
  let fixture: ComponentFixture<DismissiveAlertComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DismissiveAlertComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DismissiveAlertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
