import { Component, OnInit, ViewChild } from '@angular/core';
import { CompanyService } from 'src/app/services/company/company.service';
import { UserType } from 'src/app/shared/enums/UserType-enum.model';
import { Company } from 'src/app/shared/models/Company.model';
import { User } from 'src/app/shared/models/User.model';
import { Job } from 'src/app/shared/models/Job.model';
import { DismissiveAlertComponent } from '../../utils/dismissive-alert/dismissive-alert.component';
import { UserService } from 'src/app/services/user/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-company-profile',
  templateUrl: './company-profile.component.html',
  styleUrls: ['./company-profile.component.css'],
})
export class CompanyProfileComponent implements OnInit {
  // Store company details of user | Dummy object for testing
  company!: Company;
  // Json Stringify version of company object for passing in form| Dummy object for testing
  formCompany: Company;

  // Get from parent controllers
  user: User = this.userService.loggedInUser;
  companyIdFromUser: number;

  // Check for UserType
  disableSwitching: boolean;

  // List of SPoC users | Dummy object for testing
  allUsers: User[] = [];

  buttonNameUpdate: string = 'Update';

  constructor(
    private companyService: CompanyService,
    private userService: UserService,
    private router: Router
  ) {}

  @ViewChild(DismissiveAlertComponent)
  alert!: DismissiveAlertComponent;

  ngOnInit(): void {
    // Will get companyId from user
    if (
      this.userService.loggedInUser &&
      this.userService.loggedInUser.userCompany &&
      this.userService.loggedInUser.userCompany.companyId
    ) {
      this.companyIdFromUser =
        this.userService.loggedInUser.userCompany.companyId;
    }

    this.companyService.getCompanyById(this.companyIdFromUser).subscribe({
      next: (result) => {
        this.company = result;
        //send a copy to user-detail-form
        this.formCompany = JSON.parse(JSON.stringify(this.company));
        this.alert.add('info', 'Company details fetched', 2000);
      },
      error: (err) => {
        this.alert.add('danger', 'Company details could not be fetched', 8000);
      },
      complete: () => console.info('complete'),
    });

    // Get all company users
    this.companyService.getAllCompanyUsers(this.companyIdFromUser).subscribe({
      next: (result) => {
        this.allUsers = result;
        this.alert.add('info', 'SPoC details fetched', 2000);
      },
      error: (err) => {
        this.alert.add('danger', 'SPoC details could not be fetched', 8000);
      },
      complete: () => console.info('complete'),
    });

    // Check if admin or not (By Default - false)
    if (
      this.userService.loggedInUser.userType?.toLocaleString() ==
      'COMPANY_ADMIN'
    ) {
      this.disableSwitching = true;
    }
  }

  onUpdate(company: any) {
    this.companyService.updateCompany(this.formCompany).subscribe({
      next: (result) => {
        this.company = result;
        this.alert.add('success', 'Company details updated sucessfully', 2000);

        if (
          this.userService.loggedInUser.userType?.toLocaleString() ==
          'COMPANY_ADMIN'
        ) {
          this.disableSwitching = true;
        }
      },
      error: (err) => {
        console.log(err);
        this.alert.add('danger', 'Company details could not be updated', 3000);
      },
      complete: () => console.info('complete'),
    });
  }
  approveStatus(spocUser: User) {
    if (spocUser.userAccountId) {
      this.companyService
        .approveSpocApproval(spocUser.userAccountId)
        .subscribe({
          next: (result) => {
            this.company = result;
            this.alert.add('success', 'Account Approved', 2000);
            spocUser.adminApprovalStatus = true;

            if (
              this.userService.loggedInUser.userType?.toLocaleString() ==
              'COMPANY_ADMIN'
            ) {
              this.disableSwitching = true;
            }
          },
          error: (err) => {
            console.log(err);
            this.alert.add('danger', 'Error occurred while approving', 3000);
          },
          complete: () => console.info('complete'),
        });
    }
  }
}
