import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { JobService } from 'src/app/services/job/job.service';
import { Job } from 'src/app/shared/models/Job.model';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { User } from 'src/app/shared/models/User.model';
import { AuthenticationService } from 'src/app/services/authenication/authenication.service';
import { UserService } from 'src/app/services/user/user.service';
import { DismissiveAlertComponent } from '../../utils/dismissive-alert/dismissive-alert.component';

@Component({
  selector: 'app-company-jobs-detail',
  templateUrl: './company-jobs-detail.component.html',
  styleUrls: ['./company-jobs-detail.component.css'],
})
export class CompanyJobsDetailComponent implements OnInit {
  jobs: Job[] = [];
  formjob!: Job;
  Status = '';
  appliedmessage: any;
  alreadyapplied: any;
  applied: any = [];
  nojobs: any;
  //posted:any =[];
  deleted: any;
  errormsg: any;
  successmsg: boolean;
  jobinfo: any;
  jobdata: any = {};
  updated: any;
  postjobForm: FormGroup;
  ProfileForm: FormGroup;
  id: number;
  // public isCollapsed = -1;
  editJob: number = -1;
  seekers: number = -1;

  allUsers: User[] = [];

  postedMsg: any;
  alreadyposted: any;

  allJobsFromCompany: Job[] = [];

  userCompanyId: any;

  editPostIndex: number = -1;

  @ViewChild(DismissiveAlertComponent)
  alert!: DismissiveAlertComponent;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private activeroute: ActivatedRoute,
    private jobService: JobService,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.postjobForm = this.formBuilder.group({
      jobRole: [''],
      jobDescription: [''],
      jobDomain: [''],
      jobType: [''],
      jobOpenFor: [''],
      jobLocation: [''],
      qualification: [''],
      skillSet: [''],
    });
    this.userCompanyId = this.userService.loggedInUser.userCompany?.companyId;

    this.postedjobs();
    this.getPostedJobsByLoggedInUser();
  }

  onPost(job: any) {
    job.value.jobCompanyUser = this.userCompanyId;
    this.jobService.postjob(job.value).subscribe({
      next: (result) => {
        console.log(job);
        this.jobs.push(result);
        this.allJobsFromCompany.push(result);
        //----Clear post job form------
        
        this.alert.add('info', 'Job posted successfully', 2000);
      },
      error: (err) => {
        console.log(err);
        console.log(job.value);
        this.alert.add('danger', 'Could not post job', 8000);
      },
      complete: () => console.info('complete'),
    });
  }

  postedjobs() {
    this.jobService.getpostedjobs(this.userCompanyId).subscribe({
      next: (result) => {
        this.allJobsFromCompany = result;
        this.alert.add('info', 'All posted jobs by the company fetched', 2000);
      },
      error: (err) => {
        console.log(err);
        this.alert.add(
          'danger',
          'Posted Jobs by company  could not be fetched',
          8000
        );
      },
      complete: () => console.info('complete'),
    });
  }

  getPostedJobsByLoggedInUser(): void {
    this.jobService
      .getPostedJobsByUser(this.userService.loggedInUser)
      .subscribe({
        next: (result) => {
          this.jobs = result;
          this.alert.add('info', 'All posted jobs fetched', 2000);
        },
        error: (err) => {
          console.log(err);
          this.alert.add(
            'danger',
            'Posted Job details could not be fetched',
            8000
          );
        },
        complete: () => console.info('complete'),
      });
  }

  onSave(job: Job, i: any) {
    this.editJob = -1;
    this.seekers = -1;
  }

  onedit(job: any, i: number) {
    this.editJob = i;
    this.editPostIndex = i;
  }

  onEdit(formjob: any) {
    let data = formjob.value;
    let id = this.jobs[this.editPostIndex].jobPostId;
    data.jobPostId = id;
    console.log(data);
    this.jobService.updatejob(data, id).subscribe({
      next: (result) => {
        //console.log(result);
        this.jobs[this.editPostIndex] = result;

        //linear search for jobId
        let editJobsFromCompanyIndex = this.allJobsFromCompany
          .map((job) => job.jobPostId)
          .indexOf(result.jobPostId);
        this.allJobsFromCompany[editJobsFromCompanyIndex] = result;

        this.alert.add('info', 'Job updated successfully', 2000);
        this.onSave(data, id);
        //
        this.editPostIndex = -1;
      },
      error: (err) => {
        console.log(err);
        this.alert.add(
          'danger',
          'Posted Job details could not be fetched',
          8000
        );
      },
      complete: () => console.info('complete'),
    });
  }

  Seekers(job: any) {
    this.jobService.getSeekers(job).subscribe({
      next: (result) => {
        this.allUsers = result;
        //console.log(this.allUsers);
        this.alert.add('info', 'Seekers fetched successfully', 2000);
      },
      error: (err) => {
        console.log(err);
        this.alert.add('danger', 'Seeker details could not be fetched', 8000);
      },
      complete: () => console.info('complete'),
    });
  }

  onSeekers(job: any, i: number) {
    this.seekers = i;
    this.Seekers(job);
  }
}
