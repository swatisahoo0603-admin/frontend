export interface Qualification {
  qualificationId?: number;
  qualificationName: string;
  qualificationSpecialization?: string;
}
