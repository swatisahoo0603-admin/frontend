export interface Location {
  locationId?: number;
  locationCity?: string;
  locationDistrict?: string;
  locationState?: string;
  locationPinCode?: string;
  locationCountry?: string;
}
