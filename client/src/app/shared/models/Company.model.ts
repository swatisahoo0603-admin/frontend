export interface Company {
  companyId?: number;
  companyName: string;
  companyWebsite: string;
  companyDescription: string;
  companyImage?: string;
  companyCin: string;
}
