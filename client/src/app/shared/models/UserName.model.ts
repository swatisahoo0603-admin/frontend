export interface UserName {
  firstName: string;
  middleName?: string;
  lastName: string;
}
