import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Location } from 'src/app/shared/models/Location.model';
import { ServerConfig } from 'src/app/shared/server-config';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  }),
};
@Injectable({
  providedIn: 'root',
})
export class LocationService {
  constructor(private httpClient: HttpClient) {}

  getAllLocations(): Observable<Location[]> {
    return this.httpClient.get<Location[]>(
      `${ServerConfig.serverBaseUrl}${ServerConfig.locationEndpoint}`,
      httpOptions
    );
  }
}
