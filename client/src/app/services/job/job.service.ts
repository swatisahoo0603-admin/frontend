import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { College } from 'src/app/shared/models/College.model';
import { Company } from 'src/app/shared/models/Company.model';
import { Job } from 'src/app/shared/models/Job.model';
import { User } from 'src/app/shared/models/User.model';
import { ServerConfig } from 'src/app/shared/server-config';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

@Injectable({
  providedIn: 'root',
})
export class JobService {
  user: User[];
  //job:Job;
  constructor(private http: HttpClient) {}

  getjobList(): Observable<any> {
    //: Observable<APIResponse<Jobs>>

    return this.http.get(
      `${ServerConfig.serverBaseUrl}${ServerConfig.jobPostEndpoint}`
    );
  }
  getjobbyId(jobId: number): Observable<any> {
    //: Observable<APIResponse<Jobs>>

    return this.http.get(`${ServerConfig.serverBaseUrl}` + jobId);
  }
  applyjob(job: any): Observable<any> {
    const jobId = job.jobPostId;
    return this.http.post<Job>(
      `${ServerConfig.serverBaseUrl}${ServerConfig.applyJobsByCollegeEndpoint}/${jobId}`,
      job,
      httpOptions
    );
  }

  //userId:number
  getappliedjobsbyusers(college: College) {
    const collegeId = college.collegeId;
    return this.http.get(
      `${ServerConfig.serverBaseUrl}${ServerConfig.applyJobsByCollegeEndpoint}/${collegeId}`,
      httpOptions
    );
    //return this.http.get(`${ServerConfig.serverBaseUrl}/applied/`+userId.toString,httpOptions);
  }

  getAppliedJobsByLoggedInUser(): Observable<any> {
    return this.http.get<any>(
      `${ServerConfig.serverBaseUrl}${ServerConfig.applyJobsByCollegeEndpoint}`,
      httpOptions
    );
  }

  postjob(job: any): Observable<Job> {
    return this.http.post<Job>(
      `${ServerConfig.serverBaseUrl}${ServerConfig.jobPostEndpoint}`,
      job,
      httpOptions
    );
  }

  getpostedjobs(companyId: number): Observable<Job[]> {
    return this.http.get<Job[]>(
      `${ServerConfig.serverBaseUrl}${ServerConfig.jobPostByCompanyEndpoint}/${companyId}`,
      httpOptions
    );
  }

  updatejob(data: Job, id: any): Observable<Job> {
    return this.http.put<Job>(
      `${ServerConfig.serverBaseUrl}${ServerConfig.jobPostEndpoint}/${id}`,
      data,
      httpOptions
    );
  }

  getSeekers(job: Job): Observable<User[]> {
    const jobId = job.jobPostId;
    return this.http.get<User[]>(
      `${ServerConfig.serverBaseUrl}${ServerConfig.jobApplyByCollegeEndpoint}/${jobId}`,
      httpOptions
    );
  }

  getPostedJobsByUser(user: User): Observable<Job[]> {
    let userId = user.userAccountId;
    return this.http.get<Job[]>(
      `${ServerConfig.serverBaseUrl}${ServerConfig.jobPostByCompanyUserEndpoint}/${userId}`,
      httpOptions
    );
  }

  deleteJob(jobId: number): Observable<Job> {
    //const jobId=job.jobId;
    //+jobId.toString()
    return this.http.delete<any>('http://localhost:3000/jobs/' + jobId).pipe(
      map((res: any) => {
        return res;
      })
    );
  }

  getjobsbycompany(company: Company) {
    const companyId = company.companyId;
    return this.http.get(
      `${ServerConfig.serverBaseUrl}/company` + companyId?.toString,
      httpOptions
    );
  }

  getJobsbyParams(Map: any): Observable<any> {
    const url = 'http://localhost:3000/jobs/';

    let queryParams = new HttpParams();
    queryParams.append('jobType', 1);
    queryParams.append('jobDomain', 1);
    queryParams.append('jobLocation', 1);
    queryParams.append('qualification', 1);
    queryParams.append('jobRole', 1);
    queryParams.append('jobOpenFor', 1);

    return this.http.get<any>(url, { params: queryParams });
  }
}
