import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Qualification } from 'src/app/shared/models/Qualification.model';
import { ServerConfig } from 'src/app/shared/server-config';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  }),
};
@Injectable({
  providedIn: 'root',
})
export class QualificationService {
  constructor(private httpClient: HttpClient) {}

  getAllQualifications(): Observable<Qualification[]> {
    return this.httpClient.get<Qualification[]>(
      `${ServerConfig.serverBaseUrl}${ServerConfig.qualificationEndpoint}`,
      httpOptions
    );
  }
}
