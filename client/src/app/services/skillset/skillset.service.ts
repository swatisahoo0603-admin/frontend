import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SkillSet } from 'src/app/shared/models/SkillSet.model';
import { ServerConfig } from 'src/app/shared/server-config';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  }),
};

@Injectable({
  providedIn: 'root',
})
export class SkillsetService {
  constructor(private httpClient: HttpClient) {}

  getAllSkillSet(): Observable<SkillSet[]> {
    return this.httpClient.get<SkillSet[]>(
      `${ServerConfig.serverBaseUrl}${ServerConfig.skillsetEndpoint}`,
      httpOptions
    );
  }
}
